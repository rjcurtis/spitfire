﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;
using Spitfire.ScreenManager;

namespace Spitfire.Application
{
    public class Controller
    {
        #region Variables
        private RenderWindow _gamewindow = null;
        private List<ScreenManagerBase> _screenmanagerstack = new List<ScreenManagerBase>();
        private Time _timestep = Time.Zero;
        private Color _clearcolor = Color.Black;
        private bool _stoploop = false;
        #endregion

        #region Properties
        public RenderWindow GameWindow
        {
            get
            {
                return _gamewindow;
            }
        }
        public Time TimeStep
        {
            get
            {
                return _timestep;
            }
            set
            {
                _timestep = value;
            }
        }
        public Color ClearColor
        {
            get
            {
                return _clearcolor;
            }
            set
            {
                _clearcolor = value;
            }
        }
        public bool StopLoop
        {
            get
            {
                return _stoploop;
            }
            set
            {
                _stoploop = value;
            }
        }
        #endregion

        #region Constructors
        public Controller(RenderWindow GameWindow, ScreenManagerBase InitialScreenManager, Time TimeStep, CloseMode Mode)
        {
            _gamewindow = GameWindow;
            BindWindowEvents(Mode);
            _screenmanagerstack.Add(InitialScreenManager);
            _screenmanagerstack[_screenmanagerstack.Count - 1].SwitchScreen += OnSwitchScreen;
            _screenmanagerstack[_screenmanagerstack.Count - 1].CloseScreen += OnCloseScreen;
            _screenmanagerstack[_screenmanagerstack.Count - 1].ScreenActivated();
            _timestep = TimeStep;
        }
        #endregion

        #region Functions
        private void BindWindowEvents(CloseMode Mode)
        {
            _gamewindow.KeyPressed += (sender, e) => { _screenmanagerstack[_screenmanagerstack.Count - 1].KeyPressed(_gamewindow, e); };
            _gamewindow.KeyReleased += (sender, e) => { _screenmanagerstack[_screenmanagerstack.Count - 1].KeyReleased(_gamewindow, e); };
            _gamewindow.MouseMoved += (sender, e) => { _screenmanagerstack[_screenmanagerstack.Count - 1].MouseMoved(_gamewindow, e); };
            _gamewindow.MouseButtonPressed += (sender, e) => { _screenmanagerstack[_screenmanagerstack.Count - 1].MouseButtonPressed(_gamewindow, e); };
            _gamewindow.MouseButtonReleased += (sender, e) => { _screenmanagerstack[_screenmanagerstack.Count - 1].MouseButtonReleased(_gamewindow, e); };
            if (Mode == CloseMode.Window) _gamewindow.Closed += (sender, e) => { _gamewindow.Close(); };
            else if (Mode == CloseMode.Screen) _gamewindow.Closed += (sender, e) => { _screenmanagerstack[_screenmanagerstack.Count - 1].CloseRequested(); };
        }
        private void OnSwitchScreen(ScreenManagerBase NewScreenManager)
        {
            if (_screenmanagerstack.Contains(NewScreenManager)) return;
            _screenmanagerstack[_screenmanagerstack.Count - 1].ScreenDeactivated();
            _screenmanagerstack[_screenmanagerstack.Count - 1].SwitchScreen -= OnSwitchScreen;
            _screenmanagerstack[_screenmanagerstack.Count - 1].CloseScreen -= OnCloseScreen;
            _screenmanagerstack.Add(NewScreenManager);
            _screenmanagerstack[_screenmanagerstack.Count - 1].SwitchScreen += OnSwitchScreen;
            _screenmanagerstack[_screenmanagerstack.Count - 1].CloseScreen += OnCloseScreen;
            _screenmanagerstack[_screenmanagerstack.Count - 1].ScreenActivated();
        }
        private void OnCloseScreen()
        {
            _screenmanagerstack[_screenmanagerstack.Count - 1].ScreenDeactivated();
            _screenmanagerstack[_screenmanagerstack.Count - 1].SwitchScreen -= OnSwitchScreen;
            _screenmanagerstack[_screenmanagerstack.Count - 1].CloseScreen -= OnCloseScreen;
            _screenmanagerstack.RemoveAt(_screenmanagerstack.Count - 1);
            if (_screenmanagerstack.Count <= 0) _gamewindow.Close();
            else
            {
                _screenmanagerstack[_screenmanagerstack.Count - 1].SwitchScreen += OnSwitchScreen;
                _screenmanagerstack[_screenmanagerstack.Count - 1].CloseScreen += OnCloseScreen;
                _screenmanagerstack[_screenmanagerstack.Count - 1].ScreenActivated();
            }
        }
        public void RunLoop()
        {
            Clock frameclock = new Clock();
            Time elapsedtime = Time.Zero;
            while (_gamewindow.IsOpen() && !_stoploop)
            {
                elapsedtime += frameclock.Restart();
                _gamewindow.DispatchEvents();
                if (!_gamewindow.IsOpen()) break;
                _gamewindow.Clear(_clearcolor);
                while (elapsedtime >= TimeStep)
                {
                    elapsedtime -= TimeStep;
                    _screenmanagerstack[_screenmanagerstack.Count - 1].Update(TimeStep);
                }
                if (_screenmanagerstack.Count >= 1) _screenmanagerstack[_screenmanagerstack.Count - 1].Draw(_gamewindow);
                _gamewindow.Display();
            }
        }
        #endregion
    }
}
