﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Audio;

namespace Spitfire.Resources
{
    public static class AudioManager
    {
        #region Variables
        private static ResourceManager _manager = null;
        private static Dictionary<string, List<Sound>> _soundcache = new Dictionary<string, List<Sound>>();
        private static int _soundcount = 0;
        #endregion

        #region Properties
        public static ResourceManager Manager
        {
            get
            {
                return _manager;
            }
            set
            {
                _manager = value;
            }
        }
        #endregion

        #region Functions
        public static void Play(string SoundBufferPath)
        {
            if (!_soundcache.ContainsKey(SoundBufferPath)) _soundcache.Add(SoundBufferPath, new List<Sound>());
            bool found = false;
            foreach (var sound in _soundcache[SoundBufferPath])
            {
                if (sound.Status == SoundStatus.Stopped)
                {
                    sound.Play();
                    found = true;
                    break;
                }
            }
            CleanSounds();
            if (!found && _soundcount < 256)
            {
                Sound sound = new Sound(_manager.GetSoundBuffer(SoundBufferPath));
                sound.Play();
                _soundcount += 1;
                _soundcache[SoundBufferPath].Add(sound);
            }
        }
        private static void CleanSounds()
        {
            foreach (var path in _soundcache.Keys)
            {
                List<Sound> removelist = null;
                foreach (var sound in _soundcache[path])
                {
                    if (sound.Status == SoundStatus.Stopped)
                    {
                        if (removelist == null) removelist = new List<Sound>();
                        removelist.Add(sound);
                    }
                }
                if (removelist != null)
                {
                    foreach (var sound in removelist)
                    {
                        _soundcache[path].Remove(sound);
                        _soundcount -= 1;
                        sound.Dispose();
                    }
                }
            }
        }
        #endregion
    }
}
