﻿using System;
using System.Collections.Generic;
using System.IO;
using SFML.Window;
using SFML.Graphics;
using SFML.Audio;
using ERP.FileControl;
using ERP.VFS;

namespace Spitfire.Resources
{
    public class ResourceManager
    {
        #region Variables
        private PackedFile _resourcefile = null;
        private Dictionary<string, Texture> _texturecache = new Dictionary<string, Texture>();
        private Dictionary<string, Font> _fontcache = new Dictionary<string, Font>();
        private Dictionary<string, SoundBuffer> _soundbuffercache = new Dictionary<string, SoundBuffer>();
        private Dictionary<string, MemoryStream> _resourcecache = new Dictionary<string, MemoryStream>();
        #endregion

        #region Constructors
        public ResourceManager(string ResourceFilePath)
        {
            _resourcefile = VFSLoader.LoadPackedFile(ResourceFilePath);
        }
        #endregion

        #region Functions
        public Texture GetTexture(string TexturePath)
        {
            if (_texturecache.ContainsKey(TexturePath)) return _texturecache[TexturePath];
            _texturecache.Add(TexturePath, new Texture(_resourcefile.LocateFile(TexturePath).FileData));
            _texturecache[TexturePath].Smooth = true;
            return _texturecache[TexturePath];
        }
        public Font GetFont(string FontPath)
        {
            if (_fontcache.ContainsKey(FontPath)) return _fontcache[FontPath];
            _fontcache.Add(FontPath, new Font(_resourcefile.LocateFile(FontPath).FileData));
            return _fontcache[FontPath];
        }
        public SoundBuffer GetSoundBuffer(string SoundBufferPath)
        {
            if (_soundbuffercache.ContainsKey(SoundBufferPath)) return _soundbuffercache[SoundBufferPath];
            _soundbuffercache.Add(SoundBufferPath, new SoundBuffer(_resourcefile.LocateFile(SoundBufferPath).FileData));
            return _soundbuffercache[SoundBufferPath];
        }
        public MemoryStream GetResource(string ResourcePath)
        {
            if (_resourcecache.ContainsKey(ResourcePath)) return _resourcecache[ResourcePath];
            _resourcecache.Add(ResourcePath, _resourcefile.LocateFile(ResourcePath).FileData);
            return _resourcecache[ResourcePath];
        }
        #endregion
    }
}
