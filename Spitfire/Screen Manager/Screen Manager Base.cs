﻿using System;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;
using Spitfire.Resources;

namespace Spitfire.ScreenManager
{
    public abstract class ScreenManagerBase
    {
        #region Variables
        private Vector2f _screensize = new Vector2f(0, 0);
        #endregion

        #region Properties
        public Vector2f ScreenSize
        {
            get
            {
                return _screensize;
            }
        }
        #endregion

        #region Events
        public event Action<ScreenManagerBase> SwitchScreen;
        public event Action CloseScreen;
        #endregion

        #region Constructors
        public ScreenManagerBase(Vector2f CurrentScreenSize)
        {
            _screensize = CurrentScreenSize;
        }
        #endregion

        #region Functions
        public virtual void KeyPressed(RenderTarget Target, KeyEventArgs EventArgs) { }
        public virtual void KeyReleased(RenderTarget Target, KeyEventArgs EventArgs) { }
        public virtual void MouseButtonPressed(RenderTarget Target, MouseButtonEventArgs EventArgs) { }
        public virtual void MouseButtonReleased(RenderTarget Target, MouseButtonEventArgs EventArgs) { }
        public virtual void MouseMoved(RenderTarget Target, MouseMoveEventArgs EventArgs) { }
        public virtual void ScreenActivated() { }
        public virtual void ScreenDeactivated() { }
        public virtual void CloseRequested() { OnCloseScreen(); }
        protected void OnSwitchScreen(ScreenManagerBase NewScreen) { if (SwitchScreen != null) SwitchScreen(NewScreen); }
        protected void OnCloseScreen() { if (CloseScreen != null) CloseScreen(); }
        public virtual void Update(Time DeltaTime) { MusicManager.Update(DeltaTime); }
        public abstract void Draw(RenderTarget Target);
        #endregion
    }
}
