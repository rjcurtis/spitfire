﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace Spitfire.UI.Controls
{
    public class Button : ControlBase
    {
        #region Delegates
        public delegate Vector2i TextureRectOffset(bool MouseOver, bool Pressed);
        public delegate Color TextColor(bool Enabled);
        #endregion

        #region Variables
        private bool _mouseover = false;
        private bool _pressed = false;
        private Sprite _backgroundsprite = null;
        private Text _displaytext = null;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return _displaytext.DisplayedString;
            }
            set
            {
                _displaytext.DisplayedString = value;
            }
        }
        public Texture BackgroundTexture
        {
            get
            {
                return _backgroundsprite.Texture;
            }
            set
            {
                _backgroundsprite.Texture = value;
            }
        }
        public Font Font
        {
            get
            {
                return _displaytext.Font;
            }
            set
            {
                _displaytext.Font = value;
            }
        }
        public uint FontSize
        {
            get
            {
                return _displaytext.CharacterSize;
            }
            set
            {
                _displaytext.CharacterSize = value;
            }
        }
        public IntRect TextureRect
        {
            get
            {
                return _backgroundsprite.TextureRect;
            }
            set
            {
                _backgroundsprite.TextureRect = value;
            }
        }
        #endregion

        #region Events
        public event Action<Button> ButtonClicked;
        public event TextureRectOffset TextureRectOffsetHandler;
        public event TextColor TextColorHandler;
        #endregion

        #region Contructors
        public Button(Vector2f ControlSize, Texture BackgroundTexture, IntRect TextureRect, Font Font, uint FontSize = 20)
        {
            Size = ControlSize;
            _backgroundsprite = new Sprite(BackgroundTexture, TextureRect);
            _displaytext = new Text("", Font, FontSize);
        }
        #endregion

        #region Functions
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            _backgroundsprite.Position = Position;
            _displaytext.Position = new Vector2f(Position.X + (Size.X / 2) - (_displaytext.GetGlobalBounds().Width / 2), Position.Y + (Size.Y / 2) - (_displaytext.GetGlobalBounds().Height / 1.2f));
            if (_pressed) _displaytext.Position += new Vector2f(2.5f, 2.5f);
            Vector2i textrectoffset = new Vector2i(0, 0);
            if (TextureRectOffsetHandler != null) textrectoffset = TextureRectOffsetHandler(_mouseover, _pressed);
            if (TextColorHandler != null) _displaytext.Color = TextColorHandler(Enabled);
            TextureRect = new IntRect(TextureRect.Left + textrectoffset.X, TextureRect.Top + textrectoffset.Y, TextureRect.Width, TextureRect.Height);
            Target.Draw(_backgroundsprite, States);
            Target.Draw(_displaytext, States);
            TextureRect = new IntRect(TextureRect.Left - textrectoffset.X, TextureRect.Top - textrectoffset.Y, TextureRect.Width, TextureRect.Height);
            base.Draw(Target, States);
        }
        public override void MouseButtonPressed(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            _pressed = true;
        }
        public override void MouseButtonReleased(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            _pressed = false;
            Vector2f convertedpos = GetMouseRelativePosition(new Vector2f(EventArgs.X, EventArgs.Y));
            if (convertedpos.X >= 0 && convertedpos.Y >= 0 && convertedpos.X <= Size.X && convertedpos.Y <= Size.Y)
            {
                if (ButtonClicked != null) ButtonClicked(this);
            }
        }
        public override void MouseEnter()
        {
            _mouseover = true;
            base.MouseEnter();
        }
        public override void MouseLeave()
        {
            _mouseover = false;
            base.MouseLeave();
        }
        #endregion
    }
}
