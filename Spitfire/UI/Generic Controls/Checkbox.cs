﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace Spitfire.UI.Controls
{
    public class Checkbox : ControlBase
    {
        #region Delegates
        public delegate Vector2i TextureRectOffset(bool MouseOver, bool Checked);
        #endregion

        #region Variables
        private bool _mouseover = false;
        private bool _checked = false;
        private Sprite _backgroundsprite = null;
        #endregion

        #region Properties
        public bool Checked
        {
            get
            {
                return _checked;
            }
            set
            {
                _checked = value;
            }
        }
        public Texture BackgroundTexture
        {
            get
            {
                return _backgroundsprite.Texture;
            }
            set
            {
                _backgroundsprite.Texture = value;
            }
        }
        public IntRect TextureRect
        {
            get
            {
                return _backgroundsprite.TextureRect;
            }
            set
            {
                _backgroundsprite.TextureRect = value;
            }
        }
        #endregion

        #region Events
        public event Action<Checkbox, bool> CheckChanged;
        public event TextureRectOffset TextureRectOffsetHandler;
        #endregion

        #region Contructors
        public Checkbox(Vector2f ControlSize, Texture BackgroundTexture, IntRect TextureRect)
        {
            Size = ControlSize;
            _backgroundsprite = new Sprite(BackgroundTexture, TextureRect);
        }
        #endregion

        #region Functions
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            _backgroundsprite.Position = Position;
            Vector2i textrectoffset = new Vector2i(0, 0);
            if (TextureRectOffsetHandler != null) textrectoffset = TextureRectOffsetHandler(_mouseover, _checked);
            TextureRect = new IntRect(TextureRect.Left + textrectoffset.X, TextureRect.Top + textrectoffset.Y, TextureRect.Width, TextureRect.Height);
            Target.Draw(_backgroundsprite, States);
            TextureRect = new IntRect(TextureRect.Left - textrectoffset.X, TextureRect.Top - textrectoffset.Y, TextureRect.Width, TextureRect.Height);
            base.Draw(Target, States);
        }
        public override void MouseEnter()
        {
            _mouseover = true;
            base.MouseEnter();
        }
        public override void MouseLeave()
        {
            _mouseover = false;
            base.MouseLeave();
        }
        public override void MouseButtonReleased(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            Vector2f convertedpos = GetMouseRelativePosition(new Vector2f(EventArgs.X, EventArgs.Y));
            if (convertedpos.X >= 0 && convertedpos.Y >= 0 && convertedpos.X <= Size.X && convertedpos.Y <= Size.Y)
            {
                _checked = !_checked;
                if (CheckChanged != null) CheckChanged(this, _checked);
            }
        }
        #endregion
    }
}
