﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace Spitfire.UI.Controls
{
    public class Label : ControlBase
    {
        #region Variables
        private Sprite _backgroundsprite = null;
        private Text _displaytext = null;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return _displaytext.DisplayedString;
            }
            set
            {
                _displaytext.DisplayedString = value;
            }
        }
        public Texture BackgroundTexture
        {
            get
            {
                return _backgroundsprite.Texture;
            }
            set
            {
                _backgroundsprite.Texture = value;
            }
        }
        public Font Font
        {
            get
            {
                return _displaytext.Font;
            }
            set
            {
                _displaytext.Font = value;
            }
        }
        public uint FontSize
        {
            get
            {
                return _displaytext.CharacterSize;
            }
            set
            {
                _displaytext.CharacterSize = value;
            }
        }
        public IntRect TextureRect
        {
            get
            {
                return _backgroundsprite.TextureRect;
            }
            set
            {
                _backgroundsprite.TextureRect = value;
            }
        }
        #endregion

        #region Contructors
        public Label(Vector2f ControlSize, Texture BackgroundTexture, IntRect TextureRect, Font Font, uint FontSize = 20)
        {
            Size = new Vector2f(175, 35);
            _backgroundsprite = new Sprite(BackgroundTexture, TextureRect);
            _displaytext = new Text("", Font, FontSize);
        }
        #endregion

        #region Functions
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            _backgroundsprite.Position = Position;
            _displaytext.Position = new Vector2f(Position.X + (Size.X / 2) - (_displaytext.GetGlobalBounds().Width / 2), Position.Y + (Size.Y / 2) - (_displaytext.GetGlobalBounds().Height / 1.2f));
            Target.Draw(_backgroundsprite, States);
            Target.Draw(_displaytext, States);
            base.Draw(Target, States);
        }
        #endregion
    }
}
