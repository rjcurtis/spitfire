﻿using System;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;

namespace Spitfire.UI.Controls
{
    public class Textbox : ControlBase
    {
        #region Consts
        private readonly Time FlashTime = Time.FromMilliseconds(500);
        #endregion

        #region Delegates
        public delegate Color TextColor(bool Enabled);
        #endregion

        #region Variables
        private Sprite _backgroundsprite = null;
        private Text _displaytext = null;
        private Text _cursor = null;
        private bool _gotfocus = false;
        private bool _cursorvisible = true;
        private Time _elapsedflashtime = Time.Zero;
        private Vector2f _textoffset = new Vector2f(10, 5);
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return _displaytext.DisplayedString;
            }
            set
            {
                _displaytext.DisplayedString = value;
            }
        }
        public Texture BackgroundTexture
        {
            get
            {
                return _backgroundsprite.Texture;
            }
            set
            {
                _backgroundsprite.Texture = value;
            }
        }
        public Font Font
        {
            get
            {
                return _displaytext.Font;
            }
            set
            {
                _displaytext.Font = value;
                _cursor.Font = value;
            }
        }
        public uint FontSize
        {
            get
            {
                return _displaytext.CharacterSize;
            }
            set
            {
                _displaytext.CharacterSize = value;
                _cursor.CharacterSize = value;
            }
        }
        public IntRect TextureRect
        {
            get
            {
                return _backgroundsprite.TextureRect;
            }
            set
            {
                _backgroundsprite.TextureRect = value;
            }
        }
        public Vector2f TextOffset
        {
            get
            {
                return _textoffset;
            }
            set
            {
                _textoffset = value;
            }
        }
        #endregion

        #region Events
        public event Action<Textbox, string> TextChanged;
        public event TextColor TextColorHandler;
        #endregion

        #region Constructors
        public Textbox(Vector2f ControlSize, Texture BackgroundTexture, IntRect TextureRect, Font Font, uint FontSize = 20)
        {
            Size = ControlSize;
            _backgroundsprite = new Sprite(BackgroundTexture, TextureRect);
            _displaytext = new Text("", Font, FontSize);
            _cursor = new Text("|", Font, FontSize);
        }
        #endregion

        #region Functions
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            _backgroundsprite.Position = Position;
            Target.Draw(_backgroundsprite, States);
            _displaytext.Position = Position + TextOffset;
            Color textcolor = Color.Black;
            if (TextColorHandler != null) textcolor = TextColorHandler(Enabled);
            if (Enabled) { _backgroundsprite.Color = Color.White; }
            else { _backgroundsprite.Color = new Color(175, 175, 175); }
            _displaytext.Color = textcolor;
            _cursor.Color = textcolor;
            if (Text.Length > 0) { _cursor.Position = new Vector2f(Position.X + TextOffset.X + _displaytext.GetGlobalBounds().Width, Position.Y + TextOffset.Y); }
            else { _cursor.Position = Position + TextOffset; }
            Target.Draw(_displaytext, States);
            if (_gotfocus && _cursorvisible) Target.Draw(_cursor, States);
            base.Draw(Target, States);
        }
        public override void GotFocus()
        {
            _gotfocus = true;
            _elapsedflashtime = Time.Zero;
            _cursorvisible = true;
            base.GotFocus();
        }
        public override void LostFocus()
        {
            _gotfocus = false;
            base.LostFocus();
        }
        public override void Update(Time DeltaTime)
        {
            _elapsedflashtime += DeltaTime;
            while (_elapsedflashtime >= FlashTime)
            {
                _elapsedflashtime -= FlashTime;
                _cursorvisible = !_cursorvisible;
            }
            base.Update(DeltaTime);
        }
        public override void KeyPressed(RenderTarget Target, KeyEventArgs EventArgs)
        {
            string newcharacter = "";
            if (EventArgs.Code == Keyboard.Key.A) { newcharacter = "a"; }
            else if (EventArgs.Code == Keyboard.Key.B) { newcharacter = "b"; }
            else if (EventArgs.Code == Keyboard.Key.C) { newcharacter = "c"; }
            else if (EventArgs.Code == Keyboard.Key.D) { newcharacter = "d"; }
            else if (EventArgs.Code == Keyboard.Key.E) { newcharacter = "e"; }
            else if (EventArgs.Code == Keyboard.Key.F) { newcharacter = "f"; }
            else if (EventArgs.Code == Keyboard.Key.G) { newcharacter = "g"; }
            else if (EventArgs.Code == Keyboard.Key.H) { newcharacter = "h"; }
            else if (EventArgs.Code == Keyboard.Key.I) { newcharacter = "i"; }
            else if (EventArgs.Code == Keyboard.Key.J) { newcharacter = "j"; }
            else if (EventArgs.Code == Keyboard.Key.K) { newcharacter = "k"; }
            else if (EventArgs.Code == Keyboard.Key.L) { newcharacter = "l"; }
            else if (EventArgs.Code == Keyboard.Key.M) { newcharacter = "m"; }
            else if (EventArgs.Code == Keyboard.Key.N) { newcharacter = "n"; }
            else if (EventArgs.Code == Keyboard.Key.O) { newcharacter = "o"; }
            else if (EventArgs.Code == Keyboard.Key.P) { newcharacter = "p"; }
            else if (EventArgs.Code == Keyboard.Key.Q) { newcharacter = "q"; }
            else if (EventArgs.Code == Keyboard.Key.R) { newcharacter = "r"; }
            else if (EventArgs.Code == Keyboard.Key.S) { newcharacter = "s"; }
            else if (EventArgs.Code == Keyboard.Key.T) { newcharacter = "t"; }
            else if (EventArgs.Code == Keyboard.Key.U) { newcharacter = "u"; }
            else if (EventArgs.Code == Keyboard.Key.V) { newcharacter = "v"; }
            else if (EventArgs.Code == Keyboard.Key.W) { newcharacter = "w"; }
            else if (EventArgs.Code == Keyboard.Key.X) { newcharacter = "x"; }
            else if (EventArgs.Code == Keyboard.Key.Y) { newcharacter = "y"; }
            else if (EventArgs.Code == Keyboard.Key.Z) { newcharacter = "z"; }
            else if (EventArgs.Code == Keyboard.Key.Space) { newcharacter = " "; }
            if (Keyboard.IsKeyPressed(Keyboard.Key.LShift) || Keyboard.IsKeyPressed(Keyboard.Key.RShift))
            {
                newcharacter = newcharacter.ToUpper();
            }
            else
            {
                newcharacter = newcharacter.ToLower();
            }
            if (EventArgs.Code != Keyboard.Key.Back && newcharacter != "" && Text.Length < 16)
            {
                Text += newcharacter;
                if (TextChanged != null) TextChanged(this, Text);
            }
            else if (EventArgs.Code == Keyboard.Key.Back && Text.Length > 0)
            {
                Text = Text.Remove((Text.Length - 1), 1);
                if (TextChanged != null) TextChanged(this, Text);
            }
        }
        #endregion
    }
}
