﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.ScreenManager;
using Spitfire.Resources;

namespace Spitfire.UI
{
    public abstract class ScreenManagerUIBase : ScreenManagerBase
    {
        #region Variables
        private ControlBase _controlcontainer = null;
        private ResourceManager _resourcemanager = null;
        private bool _uibuilt = false;
        #endregion

        #region Contructors
        public ScreenManagerUIBase(Vector2f CurrentScreenSize, ResourceManager CurrentResourceManager)
            : base(CurrentScreenSize)
        {
            _resourcemanager = CurrentResourceManager;
            _controlcontainer = new ControlBase();
            _controlcontainer.Size = CurrentScreenSize;
        }
        #endregion

        #region Properties
        protected ResourceManager ResourceManager
        {
            get
            {
                return _resourcemanager;
            }
        }
        #endregion

        #region Functions
        protected abstract void BuildUI();
        public override void ScreenActivated()
        {
            if (!_uibuilt)
            {
                BuildUI();
                _uibuilt = true;
            }
        }
        protected void AddControl(ControlBase Control)
        {
            _controlcontainer.AddChild(Control);
        }
        protected ControlBase FindControl(object Tag)
        {
            return _controlcontainer.FindChild(Tag);
        }
        protected void RemoveControl(ControlBase Control)
        {
            _controlcontainer.RemoveChild(Control);
        }
        public override void KeyPressed(RenderTarget Target, KeyEventArgs EventArgs)
        {
            _controlcontainer.OnKeyPressed(Target, EventArgs);
        }
        public override void KeyReleased(RenderTarget Target, KeyEventArgs EventArgs)
        {
            _controlcontainer.OnKeyReleased(Target, EventArgs);
        }
        public override void MouseButtonPressed(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            _controlcontainer.OnMouseButtonPressed(Target, EventArgs);
        }
        public override void MouseButtonReleased(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            _controlcontainer.OnMouseButtonReleased(Target, EventArgs);
        }
        public override void MouseMoved(RenderTarget Target, MouseMoveEventArgs EventArgs)
        {
            _controlcontainer.OnMouseMoved(Target, EventArgs);
        }
        public override void Update(NetEXT.TimeFunctions.Time DeltaTime)
        {
            _controlcontainer.Update(DeltaTime);
            base.Update(DeltaTime);
        }
        public override void Draw(RenderTarget Target)
        {
            _controlcontainer.Draw(Target, RenderStates.Default);
        }
        #endregion
    }
}
