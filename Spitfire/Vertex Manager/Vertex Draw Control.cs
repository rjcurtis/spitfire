﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;

namespace Spitfire.VertexManager
{
    public class VertexDrawControl : ControlBase
    {
        #region Variables
        private VertexArrayManager _arraymanager = null;
        #endregion

        #region Properties
        public VertexArrayManager ArrayManager
        {
            get
            {
                return _arraymanager;
            }
            set
            {
                _arraymanager = value;
            }
        }
        #endregion

        #region Constructors
        public VertexDrawControl(VertexArrayManager ArrayManager)
        {
            Enabled = false;
            _arraymanager = ArrayManager;
        }
        #endregion

        #region Functions
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            if (_arraymanager != null) _arraymanager.Draw(Target, States);
            base.Draw(Target, States);
        }
        #endregion
    }
}
