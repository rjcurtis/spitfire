﻿using System;
using System.Linq;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace Spitfire.VertexManager
{
    public class VertexArrayManager
    {
        #region Variables
        private List<VertexSetContainer> _setcontainerlist = new List<VertexSetContainer>();
        private List<object> _keylist = new List<object>();
        #endregion

        #region Functions
        public VertexSetContainer GetVertexSetContainer(object Key, Texture DefaultArrayTexture = null, int DefaultZOrder = 0)
        {
            if (_keylist.Contains(Key)) return _setcontainerlist[_keylist.IndexOf(Key)];
            _setcontainerlist.Add(new VertexSetContainer(DefaultArrayTexture, DefaultZOrder));
            _keylist.Add(Key);
            return _setcontainerlist[_setcontainerlist.Count - 1];
        }
        public void Draw(RenderTarget Target, RenderStates States)
        {
            List<VertexSetContainer> templist = new List<VertexSetContainer>();
            templist.AddRange(_setcontainerlist);
            templist.Sort(new Comparison<VertexSetContainer>((LHS, RHS) => { return LHS.ZOrder.CompareTo(RHS.ZOrder); }));
            foreach (VertexSetContainer set in templist)
            {
                set.Draw(Target, States);
            }
        }
        #endregion
    }
}
