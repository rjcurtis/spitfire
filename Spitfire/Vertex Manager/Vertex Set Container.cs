﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace Spitfire.VertexManager
{
    public class VertexSetContainer : Drawable
    {
        #region Variables
        private List<VertexSet> _needsupdatesetlist = new List<VertexSet>();
        private List<VertexSet> _setlist = new List<VertexSet>();
        private VertexArray _array = new VertexArray(PrimitiveType.Quads);
        private Texture _arraytexture = null;
        private int _zorder = 0;
        #endregion

        #region Properties
        public Texture ArrayTexture
        {
            get
            {
                return _arraytexture;
            }
            set
            {
                _arraytexture = value;
            }
        }
        public int ZOrder
        {
            get
            {
                return _zorder;
            }
            set
            {
                _zorder = value;
            }
        }
        #endregion

        #region Constructors
        public VertexSetContainer(Texture ArrayTexture = null, int ZOrder = 0)
        {
            _arraytexture = ArrayTexture;
            _zorder = ZOrder;
        }
        #endregion

        #region Functions
        public void VertexSetUpdated(VertexSet Set)
        {
            if (!_needsupdatesetlist.Contains(Set)) _needsupdatesetlist.Add(Set);
        }
        public void AddVertexSet(VertexSet Set)
        {
            if (_setlist.Contains(Set)) return;
            _setlist.Add(Set);
        }
        public void RemoveVertexSet(VertexSet Set)
        {
            if (!_setlist.Contains(Set)) return;
            _setlist.Remove(Set);
            if (_needsupdatesetlist.Contains(Set)) _needsupdatesetlist.Remove(Set);
            if (Set.ExistingIndex != -1)
            {
                foreach (VertexSet set in _setlist)
                {
                    if (set.ExistingIndex > Set.ExistingIndex)
                    {
                        set.ExistingIndex = -1;
                        if (!_needsupdatesetlist.Contains(set)) _needsupdatesetlist.Add(set);
                    }
                }
                _array.Resize((uint)Set.ExistingIndex);
            }
            Set.ExistingIndex = -1;
        }
        public void ClearVertexSets()
        {
            _needsupdatesetlist.Clear();
            _setlist.Clear();
            _array.Clear();
        }
        public void Draw(RenderTarget Target, RenderStates States)
        {
            UpdateArray();
            if (_arraytexture != null) States.Texture = _arraytexture;
            Target.Draw(_array, States);
        }
        private void UpdateArray()
        {
            foreach (VertexSet set in _needsupdatesetlist)
            {
                if (set.ExistingIndex != -1)
                {
                    _array[(uint)set.ExistingIndex] = set[0];
                    _array[(uint)set.ExistingIndex + 1] = set[1];
                    _array[(uint)set.ExistingIndex + 2] = set[2];
                    _array[(uint)set.ExistingIndex + 3] = set[3];
                }
                else
                {
                    set.ExistingIndex = (int)_array.VertexCount;
                    _array.Append(set[0]);
                    _array.Append(set[1]);
                    _array.Append(set[2]);
                    _array.Append(set[3]);
                }
            }
            _needsupdatesetlist.Clear();
        }
        #endregion
    }
}
